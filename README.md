# Postman collection generator for IndieAuth

As detailed in [Autogenerating Postman Collections for IndieAuth](https://www.jvt.me/posts/2021/02/04/indieauth-postman/), this is a Ruby tool to allow generating a Postman collection for an IndieAuth server.

## Usage

```sh
bundle config set path vendor/bundle
bundle install
bundle exec ruby local.rb https://www.staging.jvt.me
```
