require 'sinatra'

require_relative './lib'

get '/' do
  erb :index
end

get '/collection' do
  profile_config = get_profile_config params['profile_url']

  content_type :json
  build_collection(profile_config).to_json
end
