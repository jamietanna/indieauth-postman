require 'json'
require 'net/http'
require 'microformats'
require 'securerandom'

CLIENT_ID = 'https://micropub-indieauth.herokuapp.com'
REDIRECT_URI = 'http://localhost:12345/callback'

# via https://www.jvt.me/posts/2019/09/07/ruby-hash-keys-string-symbol/
class ::Hash
  # via https://stackoverflow.com/a/25835016/2257038
  def stringify_keys
    h = self.map do |k,v|
      v_str = if v.instance_of? Hash
                v.stringify_keys
              else
                v
              end

      [k.to_s, v_str]
    end
    Hash[h]
  end

  # via https://stackoverflow.com/a/25835016/2257038
  def symbol_keys
    h = self.map do |k,v|
      v_sym = if v.instance_of? Hash
                v.symbol_keys
              else
                v
              end

      [k.to_sym, v_sym]
    end
    Hash[h]
  end
end

def authorization_request(me)
  req = {
    name: 'Initiate an Authorization Request',
    request: {
      method: 'GET',
      description: "Request the user provide their authorization for the requested scopes. You will likely want to perform this in a browser, rather than in Postman, as it will require the user to perform specific actions.",
      headers: [],
      url: {
        host: %w({{authorizationEndpoint}}),
        query: [
          {
            key: 'redirect_uri',
            value: '{{redirectUri}}',
          },
          {
            key: 'client_id',
            value: '{{clientId}}',
          },
          {
            key: 'scope',
            value: 'create update delete undelete media',
          },
          {
            key: 'response_type',
            value: 'code',
          },
          {
            key: 'me',
            value: me,
            disabled: true,
          },
          {
            key: 'code_challenge',
            value: '{{codeChallenge}}',
          },
          {
            key: 'code_challenge_method',
            value: '{{codeChallengeMethod}}',
          },
        ],
      },
    },
    event: [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Query responds successfully\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200]);",
            "});"
          ],
          "type": "text/javascript"
        }
      },
      {
        "listen": "prerequest",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.globals.set(\"codeVerifier\", pm.variables.replaceIn(\"{{$guid}}-{{$guid}}\"));",
            "var rawCodeChallenge = CryptoJS.SHA256(pm.variables.replaceIn(\"{{codeVerifier}}\"));",
            "var b64CodeChallenge = CryptoJS.enc.Base64.stringify(rawCodeChallenge)",
            "var codeChallenge = b64CodeChallenge.replace(/\\+/g, '-').replace(/\\//g, '_').replace(/=/g, '');",
            "pm.globals.set(\"codeChallenge\", codeChallenge);",
            "pm.globals.set(\"codeChallengeMethod\", \"S256\")"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }.symbol_keys

  req
end

def exchange_authorization_code
  {
    name: "Exchange Authorization Code",
    request: {
      body: {
        mode: 'urlencoded',
        urlencoded: [
          {
            key: 'grant_type',
            value: 'authorization_code',
            type: 'text',
          },
          {
            key: 'client_id',
            value: '{{clientId}}',
            type: 'text',
          },
          {
            key: 'redirect_uri',
            value: '{{redirectUri}}',
            type: 'text',
          },
          {
            key: 'code',
            value: '{{authorizationCode}}',
            type: 'text',
          },
          {
            key: 'code_verifier',
            value: '{{codeVerifier}}',
            type: 'text',
          },
        ],
      },
      method: 'POST',
      description: "Exchange the authorization code at the token endpoint for an access token, and optionally refresh token. See https://indieauth.spec.indieweb.org/#redeeming-the-authorization-code for more details",
      headers: [],
      url: {
        host: %w({{tokenEndpoint}}),
        query: [],
        raw: '{{tokenEndpoint}}',
      },
    },
    "event": [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Authorization code is exchanged succesfully, and token(s) are returned\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200]);",
            "  var response = pm.response.json();",
            "  pm.globals.set(\"me\", response.me);",
            "  pm.globals.set(\"accessToken\", response.access_token);",
            "  if (null != response.refresh_token) {",
            "    pm.globals.set(\"refreshToken\", response.refresh_token);",
            "  }",
            "});"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }
end

def profile_from_authorization_code
  {
    name: "Retrieve Profile URL from Authorization Code",
    request: {
      body: {
        mode: 'urlencoded',
        urlencoded: [
          {
            key: 'grant_type',
            value: 'authorization_code',
            type: 'text',
          },
          {
            key: 'client_id',
            value: '{{clientId}}',
            type: 'text',
          },
          {
            key: 'redirect_uri',
            value: '{{redirectUri}}',
            type: 'text',
          },
          {
            key: 'code',
            value: '{{authorizationCode}}',
            type: 'text',
          },
          {
            key: 'code_verifier',
            value: '{{codeVerifier}}',
            type: 'text',
          },
        ],
      },
      method: 'POST',
      description: "Exchange the authorization code at the authorization endpoint to determine the user's profile URL. See https://indieauth.spec.indieweb.org/#redeeming-the-authorization-code for more details",
      headers: [],
      url: {
        host: %w({{authorizationEndpoint}}),
        query: [],
        raw: '{{authorizationEndpoint}}',
      },
    },
    "event": [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Authorization code is exchanged succesfully, and token(s) are returned\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200]);",
            "  var response = pm.response.json();",
            "  pm.globals.set(\"me\", response.me);",
            "});"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }

end

def verify_access_token
  {
    "name": "Verify Access Token",
    "item": [
      {
        name: "Token Verification (IndieAuth)",
        description: 'Verify that the access token is valid. See https://indieauth.spec.indieweb.org/#access-token-verification for more details',
        "request": {
          "auth": {
            "type": "bearer",
            "bearer": [
              {
                "key": "token",
                "value": "{{accessToken}}",
                "type": "string"
              }
            ]
          },
          "method": "GET",
          "header": [],
          "url": {
            "raw": "{{tokenEndpoint}}",
            "host": [
              "{{tokenEndpoint}}"
            ]
          }
        },
        "event": [
          {
            "listen": "test",
            "script": {
              "id": SecureRandom.uuid,
              "exec": [
                "pm.test(\"Token is verified successfully\", () => {",
                "  pm.expect(pm.response.code).to.be.oneOf([200]);",
                "});"
              ],
              "type": "text/javascript"
            }
          }
        ],
      },
      {
        name: "Token Verification (OAuth2)",
        request: {
          body: {
            mode: 'urlencoded',
            urlencoded: [
              {
                key: 'token',
                value: '{{accessToken}}',
                type: 'text',
              },
            ],
          },
          method: 'POST',
          description: 'Verify that the access token is valid. See https://www.oauth.com/oauth2-servers/token-introspection-endpoint/ for more details',
          headers: [],
          url: {
            host: %w({{tokenVerifyEndpoint}}),
            query: [],
            raw: '{{tokenVerifyEndpoint}}',
          },
        },
        "event": [
          {
            "listen": "test",
            "script": {
              "id": SecureRandom.uuid,
              "exec": [
                "pm.test(\"Token is verified successfully\", () => {",
                "  pm.expect(pm.response.code).to.be.oneOf([200]);",
                "});"
              ],
              "type": "text/javascript"
            }
          }
        ],
      }
    ]
  }
end

def refresh_token_grant
  {
    "name": "Refresh Token",
    "item": [
      {
        name: "Retrieve a new Access Token",
        request: {
          body: {
            mode: 'urlencoded',
            urlencoded: [
              {
                key: 'grant_type',
                value: 'refresh_token',
                type: 'text',
              },
              {
                key: 'client_id',
                value: '{{clientId}}',
                type: 'text',
              },
              {
                key: 'refresh_token',
                value: '{{refreshToken}}',
                type: 'text',
              },
            ],
          },
          method: 'POST',
          description: "Retrieve a new access token, and optionally a new refresh token, by sending a refresh token to the token endpoint",
          headers: [],
          url: {
            host: %w({{tokenEndpoint}}),
            query: [],
            raw: '{{tokenEndpoint}}',
          },
        },
        "event": [
          {
            "listen": "test",
            "script": {
              "id": SecureRandom.uuid,
              "exec": [
                "pm.test(\"Authorization code is exchanged succesfully, and token(s) are returned\", () => {",
                "  pm.expect(pm.response.code).to.be.oneOf([200]);",
                "  var response = pm.response.json();",
                "  pm.globals.set(\"me\", response.me);",
                "  pm.globals.set(\"accessToken\", response.access_token);",
                "  if (null != response.refresh_token) {",
                "    pm.globals.set(\"refreshToken\", response.refresh_token);",
                "  }",
                "});"
              ],
              "type": "text/javascript"
            }
          }
        ],
      }
    ]
  }
end

def build_collection(profile_config)
  {
    "info": {
      "_postman_id": SecureRandom.uuid,
      "name": "IndieAuth Configuration for #{profile_config[:profile_url]}",
      "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
    },
    "item": [
      {
        "name": "Authorization",
        "item": [
          authorization_request(profile_config[:profile_url]),
          exchange_authorization_code,
          profile_from_authorization_code,
        ],
      },
      verify_access_token, # both OAuth2 and IndieAuth
      refresh_token_grant,
    ],
    "auth": {
      "type": "oauth2",
      "oauth2": [
        {
          "key": "code_verifier",
          "value": "",
          "type": "string"
        },
        {
          "key": "addTokenTo",
          "value": "header",
          "type": "string"
        },
        {
          "key": "clientId",
          "value": "{{clientId}}",
          "type": "string"
        },
        {
          "key": "useBrowser",
          "value": true,
          "type": "boolean"
        },
        {
          "key": "accessTokenUrl",
          "value": "{{tokenEndpoint}}",
          "type": "string"
        },
        {
          "key": "authUrl",
          "value": "{{authorizationEndpoint}}",
          "type": "string"
        },
        {
          "key": "tokenName",
          "value": "IndieAuth token",
          "type": "string"
        },
        {
          "key": "scope",
          "value": "create read update delete",
          "type": "string"
        },
        {
          "key": "client_authentication",
          "value": "header",
          "type": "string"
        },
        {
          "key": "grant_type",
          "value": "authorization_code_with_pkce",
          "type": "string"
        }
      ]
    },
    "variable": [
      {
        "id": SecureRandom.uuid,
        "key": "authorizationEndpoint",
        "value": profile_config[:authorization_endpoint],
      },
      {
        "id": SecureRandom.uuid,
        "key": "tokenEndpoint",
        "value": profile_config[:token_endpoint],
      },
      {
        "id": SecureRandom.uuid,
        "key": "tokenVerifyEndpoint",
        "value": '',
      },
      {
        "id": SecureRandom.uuid,
        "key": "clientId",
        "value": CLIENT_ID,
      },
      {
        "id": SecureRandom.uuid,
        "key": "redirectUri",
        "value": REDIRECT_URI,
      },
    ],
  }
end

def get(url)
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true if uri.instance_of? URI::HTTPS
  request = Net::HTTP::Get.new(uri.request_uri)
  http.request(request)
end

def get_profile_config(profile_url)
  res = get(profile_url)

  parsed = Microformats.parse(res.body)
  {
    profile_url: profile_url,
    authorization_endpoint: parsed.rels['authorization_endpoint'][0],
    token_endpoint: parsed.rels['token_endpoint'][0],
  }
end
